package nucleus.core;

/**
 * ...
 * @author milgo
 */
class Action
{
	public var name:String;
	public var value:Dynamic;
	public var parent:Dynamic;
	
	public function new(name:String, ?value:Dynamic, ?parent:Dynamic) 
	{
		this.name = name;
		this.value = value;
		this.parent = parent;
	}
	
}