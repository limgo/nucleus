package nucleus.core;
import flixel.FlxG;
import flixel.FlxState;
import haxe.Timer;
import nucleus.gui.Control;
import nucleus.gui.MessageBox;

/**
 * ...
 * @author milgo
 */

class GameState extends FlxState
{
	
	/* Screens */	
	var screens:Map<String, Screen> = new Map<String, Screen>();
	
	function addScreen(name:String, screen:Screen):Screen {
		screens.set(name, screen);
		add(screen);
		return screen;
	}
	
	function hideAllScreens() {
		for (screen in screens) {
			screen.disable();
		}
	}

	function transitionScreen(name:String, ?hidePrevScreen:Bool = true) {
		
		var time:Int = 0;
		Mouse.enable = false;
		if(hidePrevScreen){
			for (screen in screens) {
				if (screen.visible) {
					screen.animateOut(function() { screen.disable(); });
					time = 1000;
				}
			}
		}
		trace(time);
		Timer.delay(function(){
			screens[name].init();
			screens[name].animateIn(function() { Mouse.enable = true; } );
		}, time);
	}
	
	function setScreensEnable(enable:Bool) {
		for (screen in screens) {
			Screen.forEachControl(screen, function(c:Control) { c.enable = enable; } );
		}
	}
	
	function showMessageBox(props:Dynamic):MessageBox {
		var ref = null;
		var msgBox:MessageBox;
		setScreensEnable(false);
		ref = add(msgBox = new MessageBox( {
			labelFont: props.labelFont, 
			buttonFont: props.buttonFont,
			isCancelButton: props.isCancelButton,
			agreeCallback:
				function() { 
					this.remove(ref); 
					setScreensEnable(true);
					if (props.agreeCallback != null)
						props.agreeCallback();
				},
			cancelCallback:
				function() {
					this.remove(ref); 
					setScreensEnable(true);
					if (props.cancelCallback != null)
						props.cancelCallback();
				},
			msg:props.msg,
			slice9asset:props.slice9asset
		}));
		
		return msgBox;
	}
	
	public function new() 
	{
		super();
	}

}