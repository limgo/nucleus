package nucleus.core;
import flash.display.Sprite;
import flash.display.DisplayObject;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;    
import flash.display.LoaderInfo;
import flash.system.LoaderContext;
import flash.net.URLRequest;
import flash.events.Event;              
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;
import flixel.FlxG;
import flash.system.Security;
import flixel.FlxSprite;
/**
 * ...
 * @author ...
 */

class FlxWebSprite extends FlxSprite
{
	public var loaded:Bool = false;
	public var url:String = "";
	
	public function new(url:String="") 
	{
		super();
		this.url = url;
		loadGraphic("assets/images/default-profile-icon.png");
	}
	
	public function loadFromUrl(callback:Void->Void = null) {
		
		if (loaded) return;

		#if TEST
		loaded = true;
		if(callback != null)
			callback();
		#end
		
		#if !TEST
		var lctx:LoaderContext = new LoaderContext(true);
		var loader:Loader = new Loader();
		
		loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event) { 
				
			pixels = cast(cast(e.target, LoaderInfo).content, Bitmap).bitmapData;
			loaded = true;
			if(callback != null)
				callback(); 
				
		});

		if (this.url != "")
		{
			loader.load(new URLRequest(this.url));
		}
		else {
			if(callback != null)
				callback();
		}
		#end
	}
}