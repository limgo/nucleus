package nucleus.core;

/**
 * @author milgo
 */

interface Reusable 
{
	public function init():Void;
	public function disable():Void;
}