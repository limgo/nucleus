package nucleus.core;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;
import flixel.util.FlxPoint;
import nucleus.gui.Checkbox;
import nucleus.gui.Area;
import nucleus.gui.Control;
import nucleus.gui.Layer;
import nucleus.gui.ImageButton;
import nucleus.gui.Label;
import nucleus.gui.TextButton;
import nucleus.gui.TextInput;
import openfl.Assets;
import openfl.display.Sprite;
import flixel.group.FlxSpriteGroup;
import haxe.ds.Vector;

/**
 * ...
 * @author ...
 */

/* Read xml from description instead of tokenizing */
class Screen extends FlxSpriteGroup implements ActionDispatcher implements ActionListener implements Reusable
{

	private var listeners:Array<ActionListener> = new Array<ActionListener>();
	private var layers:Map<String, Layer> = new Map<String, Layer>();
	
	public function new(X:Float=0, Y:Float=0) 
	{
		super(X, Y);
		loadGUI();
	}
	
	private function getObjectByName(name:String) {
		var result:Control = null;
		
		this.forEachOfType(Layer, function(layer:Layer) {
			layer.forEachOfType(Control, function(control:Control) {
				if (control.name == name) {
					result = control;
				}
			});
		});
		
		/**/
		return result;
	}
	
	private function getLayerByName(name:String):Layer {
		var result:Layer = null;
		this.forEachOfType(Layer, function(layer:Layer) {
			if (layer.name == name) {
				result = layer;
				trace(name);
			}
		});
		return result;
	}
	
	public static function forEachControl(obj:FlxSpriteGroup, func:Control->Void ) {
		obj.forEachOfType(FlxSpriteGroup, function(group:FlxSpriteGroup) {
			if (Std.is(group, Control)) {				
				func(cast(group, Control));
				//trace("------------ " + cast(group, Control).name);				
			}
			forEachControl(group, func);
		});
	}
	
	public function init() {
		visible = true;
		//this.forEachOfType(Reusable, function(obj:Reusable) {
		//	obj.init();
		//});
		forEachControl(this, function(ctrl:Control) { ctrl.init(); } );
	}
	
	public function disable() {
		visible = false;
		//this.forEachOfType(Reusable, function(obj:Reusable) {
		//	obj.disable();
		//});
		forEachControl(this, function(ctrl:Control) { ctrl.disable(); } );
	}
	
	public function animateIn(callback:Void->Void = null) {
		//if (transition && y == -FlxG.height) {
			FlxTween.tween(this, { y: 0 }, 0.5, { ease:FlxEase.expoOut, 
					complete:function(t:FlxTween) {
						if (callback != null)
							callback();						
					}
			});
		//}
		/*else {
			if (callback != null)
				callback();
		}*/
	}
	
	public function animateOut(callback:Void->Void = null) {
		//if (transition && y == 0){
			FlxTween.tween(this, { y: -FlxG.height }, 0.5, { ease:FlxEase.expoIn, 
					complete:function(t:FlxTween) {
						if (callback != null)
							callback();
					}
			});
		/*}
		else {
			if (callback != null)
				callback();
			disable();
		}*/
	}
	
	public function addActionListener(listener:ActionListener):Void { 
		listeners.push(listener);
	}
	
	public function removeActionListener(listener:ActionListener):Void {
		listeners.remove(listener);
	}
	
	public function dispatchAction(action:Action):Void { 
		for (listener in listeners)
			listener.onAction(action);
	}
	
	public function onAction(action:Action):Void {
		
	}
	
	public function loadGUI():Void {
		
	}
	
	static function parseDescription(desc:String):Map<String, String> {
		var tokens:Array<String> = desc.split(";");
		var propMap:Map<String, String> = new Map<String, String>();
		for (i in 0...tokens.length) {
			var prop:Array<String> = tokens[i].split(":");
			propMap.set(prop[0], prop[1]);
		}
		return propMap;
	}
	
	public static function parseGUIfromSVG(filename:String, parent:Screen, callback:Control->Void=null)
	{
		var textBytes = Assets.getText("assets/images/" + filename + ".svg");
		
		var xml:Xml = Xml.parse(textBytes).firstElement();
		
		var gWidth:Float = Std.parseFloat(xml.get("width"));
		var gHeight:Float = Std.parseFloat(xml.get("height"));
		
		var layers = null;
		if (xml.elementsNamed("g").hasNext())
			layers = xml.elementsNamed("g");
		else if (xml.elementsNamed("svg:g").hasNext())
			layers = xml.elementsNamed("svg:g");
		else return;
		
		for(child in layers) 
		{
			var layer:Layer = new Layer();
			layer.name = child.get("id");
			
			for (obj in child.elements()) 
			{
				var x:Float = Std.parseFloat(obj.get("x"));
				var y:Float = Std.parseFloat(obj.get("y"));
				var w:Float = Std.parseFloat(obj.get("width"));
				var h:Float = Std.parseFloat(obj.get("height"));
				var id:String = obj.get("id");
			
				var desc:String = "";
				
				if(obj.elementsNamed("desc").hasNext())
					desc = obj.elementsNamed("desc").next().firstChild().nodeValue;
				else if(obj.elementsNamed("svg:desc").hasNext())
					desc = obj.elementsNamed("svg:desc").next().firstChild().nodeValue;	
					
				var props:Map<String, String> = parseDescription(desc);
				
				trace(obj.nodeName);
				
				switch(obj.nodeName) {
					case "svg:image", "image": {
						
						var fileName:String = obj.get("sodipodi:absref");				
						fileName = StringTools.replace(fileName, "/" , "#");
						fileName = StringTools.replace(fileName, "\\" , "#");							
						var fnarray:Array<String> = fileName.split("#");															
						var assetName:String = "assets/images/" + fnarray[fnarray.length - 1];
						
						if (props["type"] == "button")
						{											
							var button:ImageButton = new ImageButton(0, 0, assetName, assetName);
							button.x = x;
							button.y = y;
							button.setCallback(function() { parent.dispatchAction(new Action(obj.get("onclick"), null, parent)); } );
							button.name = id;
							layer.add(button);
							if (callback != null)
								callback(button);
						}
						else //image
						{
							var image:FlxSprite = new FlxSprite(0, 0, assetName);
							image.x = x;
							image.y = y;
							layer.add(image);
						}
					}
					
					case "svg:rect", "rect": {
						if (props["type"] == "area") //--------------------------
						{
							var area:Area = new Area(x, y, w, h);
							area.name = id;							
							layer.add(area);
							if (callback != null)
								callback(area);
						}
						
						if (props["type"] == "label") //--------------------------
						{				
							var origin:FlxPoint = new FlxPoint(x + w / 2, y + h / 2);							
							
							if (props.exists("valign")) 
							{						
								switch(props["valign"]){
									case "left": origin.x = x;
									case "center": origin.x = x + w / 2;
									case "right": origin.x = x + w;
								}						
							}else props.set("valign", "center");
							
							if (props.exists("halign"))
							{
								switch(props["halign"]){
									case "top": origin.y = y;
									case "center": origin.y = y + h / 2;
									case "bottom": origin.y = y + h;
								}
							}else props.set("halign", "center");
							
							var label:Label = new Label(origin.x, origin.y, props["text"], 
								props["font"], {valign:props["valign"], halign:props["halign"]});
							label.name = id;
							
							if (props.exists("color")) {
								label.setColor(Std.parseInt(props["color"]));
							}
							layer.add(label);
							if (callback != null)
								callback(label);
						}
						
						if (props["type"] == "button") //--------------------------
						{
							var button:TextButton = new TextButton(x + w / 2, y + h / 2, props["text"], 
								props["font"], { valign:"center", halign:"center" },
								function() { parent.dispatchAction(new Action(obj.get("onclick"), null, parent)); } );
							button.name = id;
							
							if (props.exists("pressedColor")) {
								button.pressedColor = Std.parseInt(props["pressedColor"]);
							}
							
							if (props.exists("releaseColor")) {
								button.releaseColor = Std.parseInt(props["releaseColor"]);
							}
							
							layer.add(button);
							if (callback != null)
								callback(button);
						}
						
						if (props["type"] == "checkbox")//--------------------------
						{
							var checkbox:Checkbox = new Checkbox(0, 0, false, function(checked:Bool) {						
								parent.dispatchAction(new Action("CHECKBOX_SELECTED", {id: id, checked: checked}, parent));
								
							});
							checkbox.x = x + w / 2 - checkbox.width / 2;
							checkbox.y = y + h / 2 - checkbox.height / 2;
							checkbox.name = id;
							layer.add(checkbox);
							if (callback != null)
								callback(checkbox);
						}
						
						if (props["type"] == "textinput")//--------------------------
						{
							var textInput:TextInput = new TextInput(x, y + h / 2, Std.int(w), props["font"], props["text"], props["text"]);
							textInput.name = id;
							layer.add(textInput);
							if (callback != null)
								callback(textInput);
						}
					}
				}
			}

			parent.layers.set(layer.name, layer);
			trace(layer.name);
			parent.add(layer);
		}

	}
	
}