package nucleus.core;

/**
 * @author milgo
 */

interface ActionListener 
{
	public function onAction(action:Action):Void;
}