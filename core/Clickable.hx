package core;

/**
 * @author milgo
 */

interface Clickable 
{
	public function onPressed():Void;
	public function onReleased():Void;
}