package nucleus.core;
import flixel.FlxG;

/**
 * ...
 * @author milgo
 */
class Mouse
{
	public static var enable:Bool = true;
	public function new() 
	{
		
	}
	
	public static function justPressed():Bool {
		return FlxG.mouse.justPressed && enable;
	}
	
	public static function justReleased():Bool {
		return FlxG.mouse.justReleased && enable;		
	}
}