package nucleus.core;

/**
 * @author milgo
 */

interface ActionDispatcher 
{
	public function addActionListener(listener:ActionListener):Void;
	public function removeActionListener(listener:ActionListener):Void;
	public function dispatchAction(action:Action):Void;
}