package nucleus.core;

import flixel.FlxSprite;

/**
 * ...
 * @author milgo
 */
class Animation extends FlxSprite
{
	public var onFinishCallback:Void->Void;
	
	public function new(X:Float = 0, Y:Float = 0) 
	{
		super();
	}
	
	override public function update():Void 
	{
		super.update();
		if (animation.finished) {
			if(onFinishCallback != null)
				onFinishCallback();
		}
	}
	
	public function play() {
		animation.play("animation");
	}
	
	public function init(frames:Array<Int>, frameRate:Int, loop:Bool) {
		animation.add("animation", frames, frameRate, loop);
	}
	
	override public function setPosition(X:Float = 0, Y:Float = 0):Void 
	{
		this.x = X - width / 2;
		this.y = Y - height / 2;
	}
	
}