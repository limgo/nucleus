package nucleus.gui;

/**
 * ...
 * @author milgo
 */
class Area extends Control
{
	var areaWidth:Float = 0.0;
	var areaHeight:Float = 0.0;
	
	public function new(X:Float=0, Y:Float=0, width:Float, height:Float) 
	{
		super(0, 0);
		x = X;
		y = Y;
		areaWidth = width;
		areaHeight = height;
	}
	
	//@:getter(width)
	override function get_width():Float 
	{
		return areaWidth;
	}
	
	//@:setter(width)
	override function set_width(value:Float):Float 
	{
		areaWidth = value;
		return areaWidth;
	}
	
	//@:getter(height)
	override function get_height():Float 
	{
		return areaHeight;
	}
	
	//@:setter(height)
	override function set_height(value:Float):Float 
	{
		areaHeight = value;
		return areaHeight;
	}
	
}