package nucleus.gui;
import flixel.addons.ui.FlxInputText;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.pxText.PxBitmapFont;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;
import nucleus.font.BitmapFonts;
import nucleus.gui.Control;
import nucleus.gui.Label;
/**
 * ...
 * @author milgo
 */
class TextInput extends Control
{
	var input:FlxInputText;
	var label:Label;
	var border:FlxSprite;
	var font:PxBitmapFont;
	var displayedText:String;
	public var defaultText:String;
	
	public function new(X:Float=0, Y:Float=0, width:Int, font:String, text:String="", ?defaultText:String = "text") 
	{
		super(X, Y);

		this.font = BitmapFonts.getFont(font);
		this.defaultText = defaultText;
		
		border = new FlxSprite(0, 0);
		border.makeGraphic(width, this.font.getFontHeight(), 0x4400ffff);
		border.y = -border.height / 2;
		add(border);
		
		displayedText = text;
		
		input = new FlxInputText(0, 0, width, text);
		input.hasFocus = true;
		add(label = new Label(0, 0, " ", font, { valign:"left", halign:"center" } ));
		
	}
	
	public function setCallback(callback:String->String->Void) {
		input.callback = function(str1:String, str2:String) {
			
			var str:String = "";
			
			if (displayedText == defaultText) {
				setText("");
				input.text = str1.charAt(str1.length - 1);
				input.caretIndex = 1;
			}
			/* Display only text that fits in text input border */
			
			var str:String = input.text;
			
			displayedText = str;
		
			var len:Int = displayedText.length;
			var textWidth = font.getTextWidth(displayedText);
			
			if (textWidth > border.width) {
				
				for (i in 0...str.length) {
					displayedText = str.substr(i);
					var w:Int = font.getTextWidth(displayedText);
					if (w < border.width) break;
				}
				
			}
			
			callback(str1, str2);
		}
	}
	
	public function setText(str:String) {
		if (str == "") 
		{
			displayedText = "";
			input.text = "";
			input.caretIndex = 0;
			label.text = " ";
		}else
		{
			displayedText = str;
			input.text = str;
			label.text = str;
			input.caretIndex = str.length;
		}
	}
	
	override public function update():Void 
	{
		super.update();
		
		if (border.overlapsPoint(FlxG.mouse) && FlxG.mouse.justPressed && displayedText == defaultText) {
			setText("");
		}
		
		if(displayedText != "")
			label.text = displayedText;
		if(displayedText == "") 
			label.text = " ";
	}
}