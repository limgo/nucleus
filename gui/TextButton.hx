package nucleus.gui;


import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.FlxG;
import nucleus.core.Mouse;
import nucleus.gui.Button;
import nucleus.gui.Label;
/**
 * ...
 * @author milgo
 */
class TextButton extends Button
{
	
	public var label:Label;
	@:isVar public var pressedColor(get, set):Int;
	@:isVar public var releaseColor(get, set):Int;
	
	public function new(x:Float = 0, y:Float = 0, label:String = "", font:String, ?textAlign:TextAlign, ?callbackFunc:Void -> Void = null)
	{
		super(x, y, callbackFunc);
		this.label = new Label(0, 0, label, font, textAlign);
		add(this.label);
	}
	
	override public function onPressed() 
	{
		super.onPressed();
		label.setColor(pressedColor);
	}

	override function update():Void 
	{
		super.update();
		
		if(FlxG.mouse.justReleased) {
			label.setColor(releaseColor);
		}
	}
	
	function get_pressedColor():Int 
	{
		return pressedColor;
	}
	
	function set_pressedColor(value:Int):Int 
	{
		return pressedColor = value;
	}
	
	function get_releaseColor():Int 
	{
		return releaseColor;
	}
	
	function set_releaseColor(value:Int):Int 
	{
		label.setColor(value);
		label.draw();
		return releaseColor = value;
	}
}