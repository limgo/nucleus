package nucleus.gui;
import flixel.FlxSprite;
import nucleus.gui.PagedWindow;
/**
 * ...
 * @author milgo
 */
class TableWindow extends PagedWindow
{
	var rows:Int;
	var cols:Int;

	var rowCounter:Int = 0;
	var colCounter:Int = 0;
	
	public function new(X:Float=0, Y:Float=0, windowWidth:Float, windowHeight:Float, rows:Int, cols:Int) 
	{
		super(X, Y, windowWidth, windowHeight);
		this.rows = rows;
		this.cols = cols;
	}

	public function addToTable(obj:FlxSprite, align:String="") {
		
		this.addToPage(obj, pageCounter, 
			colCounter * (windowWidth / cols),
			rowCounter * (windowHeight / rows));
		
		colCounter++;
		if (colCounter > cols - 1) {
			rowCounter++;
			if (rowCounter > rows - 1) {
				pageCounter++;
				rowCounter = 0;
			}
			colCounter = 0;
		}
	}
	
	public function showOnly(callback:FlxSprite-> Bool) 
	{
		rowCounter = 0;
		colCounter = 0;
		pageCounter = 0;
		
		for (i in 0...objects.length) 
		{
			if (callback(objects[i]))
			{
				objects[i].x = this.x + colCounter * (windowWidth / cols);
				objects[i].y = this.y + rowCounter * (windowHeight / rows);
				pageIndex[i] = pageCounter;
				
				colCounter++;
				if (colCounter > cols - 1) 
				{
					rowCounter++;
					if (rowCounter > rows - 1) 
					{
						pageCounter++;
						rowCounter = 0;
					}
					colCounter = 0;
				}
			}
			else {
				objects[i].visible = false;
				pageIndex[i] = -1;
			}
		}
	}
}