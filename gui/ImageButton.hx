package nucleus.gui;
import flixel.FlxG;
import flixel.FlxSprite;
import nucleus.core.Mouse;
import nucleus.gui.Button;

/**
 * ...
 * @author milgo
 */
class ImageButton extends Button
{
	var image:FlxSprite;
	var imagePressed:FlxSprite = null;	
	
	public function new(x:Float, y:Float, img:String, ?imgPressed:String = "", ?callbackFunc:Void->Void = null) 
	{
		super(x, y, callbackFunc);
		image = new FlxSprite();
		image.loadGraphic(img);
		add(image);
		
		if (imgPressed != "") {
			imagePressed = new FlxSprite();
			imagePressed.loadGraphic(imgPressed);
			add(imagePressed);
			imagePressed.visible = false;
		}
		
	}
	
	override public function init() 
	{
		super.init();
		imagePressed.visible = false;
	}
	
	override public function onPressed() 
	{
		super.onPressed();
		if (imagePressed != null)
			imagePressed.visible = true;
		
	}
	
	override function update():Void 
	{
		super.update();
		
		if(FlxG.mouse.justReleased) {
			if (imagePressed != null){
				imagePressed.visible = false;
			}			
		}
	}
	
}