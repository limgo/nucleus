package nucleus.gui;
import core.Clickable;
import flixel.group.FlxSpriteGroup;
import nucleus.core.Reusable;

/**
 * ...
 * @author milgo
 */
class Control extends FlxSpriteGroup implements Clickable implements Reusable
{
	public var name:String;
	public var enable:Bool = true;
	
	public function new(X:Float = 0, Y:Float = 0, MaxSize:Int = 0)
	{
		super(X, Y, MaxSize);
	}
	
	public function onPressed() {}
	
	public function onReleased() { }
	
	public function init() { }
	
	public function disable() { }
	
}