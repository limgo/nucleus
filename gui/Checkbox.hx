package nucleus.gui;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import nucleus.gui.Control;

/**
 * ...
 * @author milgo
 */
class Checkbox extends Control
{
	var box:FlxSprite;
	var check:FlxSprite;
	var callbackFunc:Bool->Void;
	public var checked:Bool;
	
	public function new(X:Float = 0, Y:Float = 0, ?checked:Bool = false, ?callbackFunc:Bool->Void=null )
	{
		super(X, Y, 0);
		box = new FlxSprite();
		check = new FlxSprite();
		box.loadGraphic("assets/images/checkbox-00.png");
		check.loadGraphic("assets/images/checkbox-01.png");
		add(box);
		add(check);
		this.checked = checked;
		this.callbackFunc = callbackFunc;
		
		if (checked)
			check.visible = true;
		else
			check.visible = false;
	}
	
	public function isChecked():Bool {
		return checked;
	}
	
	override function update():Void 
	{
		#if !FLX_NO_MOUSE
		if (FlxG.mouse.justReleased && overlapsPoint(FlxG.mouse, true)) 
		{
			checked = !checked;
			if(callbackFunc != null)
				callbackFunc(checked);
		}
		#end
		
		if(this.visible)
			check.visible = checked;
	}
}