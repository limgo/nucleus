package nucleus.gui;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;

/**
 * ...
 * @author milgo
 */
class PagedWindow extends FlxSpriteGroup
{
	var border:FlxSprite;
	var pageCounter:Int = 0;
		
	var pageIndex:Array<Int>;
	var objects:Array<FlxSprite>;
	var currentPage:Int = 0;
	
	public var pageCallback:FlxSprite->Void = null;
	public var windowWidth:Int;
	public var windowHeight:Int;
	
	public function new(X:Float=0, Y:Float=0, windowWidth:Float, windowHeight:Float) 
	{
		super(X, Y);
		
		pageIndex = new Array<Int>();
		objects = new Array<FlxSprite>();		
		
		this.windowWidth = Std.int(windowWidth);
		this.windowHeight = Std.int(windowHeight);
	}
	
	public function onRightArrow() 
	{
		trace("right arrow");
		currentPage++;
		if (currentPage > pageCounter - 1) {
			currentPage = pageCounter - 1;
		}
		showPage(currentPage);
	}
	
	public function onLeftArrow() 
	{
		trace("left arrow");
		currentPage--;
		if (currentPage < 0) {
			currentPage = 0;
		}
		showPage(currentPage);
	}
	
	public function addToPage(obj:FlxSprite, pageNum:Int, x:Float, y:Float, ?align:String="") 
	{
		pageIndex.push(pageNum);
		trace(pageNum);
		objects.push(obj);
		obj.x = x;
		obj.y = y;
		add(obj);
	}

	public function showPage(pageNum:Int, callback:FlxSprite->Void = null) 
	{
		if (pageNum >= 0)
		{
			for (i in 0...pageIndex.length) 
			{
				if (pageIndex[i] == pageNum) {
					objects[i].visible = true;
					if (pageCallback != null)
						pageCallback(objects[i]);
				}
				else {
					objects[i].visible = false;
				}
			}
		}
	}
}