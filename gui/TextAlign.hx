package nucleus.gui;

/**
 * @author milgo
 */

typedef TextAlign =
{
	valign:String,
	halign:String
}