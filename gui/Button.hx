package nucleus.gui;

import nucleus.core.Mouse;
import flixel.FlxG;
import flixel.group.FlxSpriteGroup;
import flixel.system.FlxSound;
import flixel.text.FlxBitmapTextField;
import flixel.text.pxText.PxBitmapFont;
import flixel.text.pxText.PxTextAlign;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import openfl.Assets;

class Button extends Control
{
	private var callbackFunc:Void->Void;
	public var clickSound:FlxSound = null;
	private var justPressed:Bool = false;
	
	override public function new(x:Float, y:Float, ?callbackFunc:Void->Void = null)
	{
		super(0, 0);
		
		this.x = x;
		this.y = y;
		
		//clickSound = FlxG.sound.load("assets/sounds/click.wav");
		
		this.callbackFunc = callbackFunc;			
	}
	
	override function update():Void 
	{
		#if !FLX_NO_MOUSE
		if (visible && enable)
		{
			if (Mouse.justPressed())
			{
				if (overlapsPoint(FlxG.mouse, true))
				{			
					onPressed();
				}
			}
			
			if (Mouse.justReleased()) 
			{
				
				if (overlapsPoint(FlxG.mouse, true)  && justPressed)
				{
					onReleased();
				}
			}
		}
		#end
	}
	
	override public function disable() 
	{
		super.disable();
		this.visible = false;
	}
	
	override public function init() 
	{
		super.init();
		this.visible = true;
	}
	
	override public function onPressed() {
		justPressed = true;
		if (clickSound != null)
			clickSound.play();
		//scale.set(0.9, 0.9);
	}
	
	override public function onReleased() 
	{
		if (justPressed) {
			if(callbackFunc != null)
				callbackFunc();
		}
		
		justPressed = false;
		//scale.set(1, 1);
	}
	
	public function setCallback(callbackFunc:Void->Void) {
		this.callbackFunc = callbackFunc;
	}
	
}