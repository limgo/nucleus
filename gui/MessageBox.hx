package nucleus.gui;

import flixel.addons.ui.FlxUI9SliceSprite;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.text.FlxBitmapTextField;
import flixel.text.pxText.PxTextAlign;
import flixel.util.FlxColor;
import nucleus.core.Action;
import nucleus.core.ActionDispatcher;
import nucleus.core.ActionListener;
import nucleus.font.BitmapFonts;
import nucleus.gui.Label;
import nucleus.gui.TextButton;
import openfl.geom.Rectangle;


/**
 * ...
 * @author milgo
 */
class MessageBox extends FlxSpriteGroup
{
	public var agreeButton:TextButton;
	public var cancelButton:TextButton;
	
	public function new(props:Dynamic) 
	{
		super();
		
		var group:FlxSpriteGroup = new FlxSpriteGroup();
		
		var background:FlxSprite = new FlxSprite();
		background.makeGraphic(FlxG.width, FlxG.height, FlxColor.BLACK);
		background.alpha = 0.6;
		add(background);
		
		var textLabel:FlxBitmapTextField = new FlxBitmapTextField(BitmapFonts.getFont(props.labelFont));
		textLabel.useTextColor = false;
		textLabel.shadow = false;		
		textLabel.width = FlxG.width * 0.6;
		textLabel.alignment = PxTextAlign.CENTER;
		textLabel.lineSpacing = 5;
		textLabel.letterSpacing = 0;
		textLabel.autoUpperCase = false;
		textLabel.wordWrap = true;
		textLabel.fixedWidth = true;
		textLabel.multiLine = true;
		textLabel.text = props.msg;
		group.add(textLabel);
		
		textLabel.draw();
		
		agreeButton = new TextButton(FlxG.width / 2, 0, "Agree", props.buttonFont, null, props.agreeCallback);
		agreeButton.releaseColor = 0x32ff32;
		agreeButton.pressedColor = FlxColor.WHITE;
		group.add(agreeButton);
		
		if (props.isCancelButton) {
			agreeButton.x = FlxG.width / 2 - FlxG.width / 12;
			cancelButton = new TextButton(FlxG.width / 2 + FlxG.width / 12, 0, "Cancel", props.buttonFont, null, props.cancelCallback);
			cancelButton.releaseColor = FlxColor.RED;
			cancelButton.pressedColor = FlxColor.WHITE;
			group.add(cancelButton);
		}
		
		textLabel.setPosition( FlxG.width * 0.2, FlxG.height / 2 - textLabel.height / 2 /*- FlxG.height * 0.05*/ - agreeButton.height / 2);
		agreeButton.y = textLabel.y + textLabel.height + agreeButton.height / 2;
		
		if(cancelButton != null)
			cancelButton.y = agreeButton.y;
			
		agreeButton.draw();
		
        var slice:Array<Int> = [16, 16, 50, 50];
		var verticalMargin = FlxG.height * 0.03;
		var horizontalMargin = FlxG.width * 0.02;
		
        var myCustomImage1 = new FlxUI9SliceSprite(FlxG.width * 0.2 - horizontalMargin, textLabel.y - verticalMargin, 
			props.slice9asset, 
			new Rectangle(0, 0, FlxG.width * 0.6 + horizontalMargin * 2, textLabel.height /*+ FlxG.height * 0.1 */+ agreeButton.height + verticalMargin * 2), slice);
		add(myCustomImage1);
		add(group);
	}
}