package nucleus.font;
import flixel.text.pxText.PxBitmapFont;
import haxe.ds.Vector;
import openfl.Assets;
/**
 * ...
 * @author milgo
 */
class BitmapFonts
{
	static var fontMap:Map<String, PxBitmapFont>;
	
	static function loadFont(fontName:String, bitmapName:String):PxBitmapFont {
		var textBytes = Assets.getText("assets/images/bitmapfonts/" + fontName + ".fnt");
		var XMLData = Xml.parse(textBytes);
		
		return new PxBitmapFont().loadAngelCode(Assets.getBitmapData("assets/images/bitmapfonts/" + bitmapName + ".png"), XMLData);
	}
	
	public static function loadFonts():Void
	{		
		var textBytes = Assets.getText("assets/images/bitmapfonts/fonts.xml");
		var xml:Xml = Xml.parse(textBytes).firstElement();
		fontMap = new Map<String, PxBitmapFont>();
		
		for (font in xml.elementsNamed("font")) {
			var name:String = font.get("name");
			trace(name);
			fontMap.set(name, loadFont(name, name));
		}
	}
	
	public static function getFont(fontName:String):PxBitmapFont {
		return fontMap.get(fontName);
	}
}